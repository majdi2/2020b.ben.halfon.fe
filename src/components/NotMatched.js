import React, { Component } from "react";

class NotMatched extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <p>No Such Page! Error 404</p>;
  }
}
export default NotMatched;
