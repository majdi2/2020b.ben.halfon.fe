import React, { Component } from "react";

import "./loginLogoutRedux.css";
import { connect } from "react-redux";
import { Dropdown, DropdownButton } from "react-bootstrap";
import Avatar from "@material-ui/core/Avatar";
import angrySmiley from "../resources/images/iconfinder_angry_1325170.png";
import robot from "../resources/images/robot.png";
import ghost from "../resources/images/ghost.png";
import ugly from "../resources/images/ugly.png";
import { signup } from "../actions/userActions";

class SignUp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      email: "",
      role: "PLAYER",

      checked: true,
      avatar: "",
    };
  }

  handleName = (e) => {
    this.setState({
      username: e.target.value,
    });
  };

  handleEmail = (e) => {
    this.setState({
      email: e.target.value,
    });
  };

  handleRole = (e) => {
    this.setState({
      role: e.target.value,
      checked: !this.state.checked,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const userData = {
      email: this.state.email,
      username: this.state.username,
      role: this.state.role,
      avatar: this.state.avatar,
    };
    this.props.dispatch(signup(userData));
  };

  render() {
    return (
      <div style={{ margingTop: "1000" }}>
        <form onSubmit={this.handleSubmit}>
          <h3>Sign Up</h3>

          <div className="form-group">
            <label>Full Name</label>
            <input
              type="text"
              className="form-control"
              placeholder="Full name"
              value={this.state.username}
              onChange={this.handleName}
            />
          </div>

          <div className="form-group">
            <label>Email address</label>
            <input
              type="email"
              className="form-control"
              placeholder="Enter email"
              value={this.state.email}
              onChange={this.handleEmail}
            />
          </div>

          <div className="role-avatar">
            <div className="radios-container">
              <label>Role</label>
              <div className="radios">
                <div className="radio-layout">
                  <input
                    type="radio"
                    value="MANAGER"
                    onChange={this.handleRole}
                    checked={!this.state.checked}
                  />
                  <label>Manager</label>
                </div>
                <div className="radio-layout">
                  <input
                    type="radio"
                    value="PLAYER"
                    onChange={this.handleRole}
                    checked={this.state.checked}
                  />
                  <label>Player</label>
                </div>
              </div>
            </div>
            <div className="avatar-container">
              <DropdownButton id="dropdown-basic-button" title="Select Avatar">
                <Dropdown.Item
                  onClick={() => this.setState({ avatar: "robot" })}
                >
                  <Avatar src={robot} />
                </Dropdown.Item>
                <Dropdown.Item
                  onClick={() => this.setState({ avatar: "angry" })}
                >
                  <Avatar src={angrySmiley} />
                </Dropdown.Item>
                <Dropdown.Item
                  onClick={() => this.setState({ avatar: "ghost" })}
                >
                  <Avatar src={ghost} />
                </Dropdown.Item>
                <Dropdown.Item
                  onClick={() => this.setState({ avatar: "ugly" })}
                >
                  <Avatar src={ugly} />
                </Dropdown.Item>
              </DropdownButton>
            </div>
          </div>

          <button type="submit" className="btn btn-primary btn-block">
            Sign Up
          </button>
        </form>
      </div>
    );
  }
}

export default connect()(SignUp);
