import React, { Component } from "react";
// import axios from "axios";
import { connect } from "react-redux";
import { login } from "../actions";
import "./loginLogoutRedux.css";

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
    };
  }

  handleEmail = (e) => {
    this.setState({
      email: e.target.value,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();

    const userData = {
      email: this.state.email,
      domain: "2020b.ben.halfon",
    };
    this.props.dispatch(login(userData));
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <h3>Log In</h3>

        <div className="form-group">
          <label>Email address</label>
          <input
            type="email"
            className="form-control"
            placeholder="Enter email"
            onChange={this.handleEmail}
            value={this.state.email}
          />
        </div>

        <button type="submit" className="btn btn-primary btn-block">
          Submit
        </button>
      </form>
    );
  }
}

export default connect()(Login);
