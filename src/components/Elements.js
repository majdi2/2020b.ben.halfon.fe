// @flow
import React, { Component } from "react";
import "./elements.css";
import Options from "../containers/Elements/index";
import { elementConstants } from "../constants/elementConstants";
import { Provider } from "../context/elementsContext";
import { connect } from "react-redux";
import axios from "../helpers/axiosHelper";
import { elementBoundary } from "../boundaries/elementBoundary";

const userLocation = {
  lat: 2,
  lng: 2,
  distance: 2,
};

class Elements extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      elements: [],
      page: 0,
      size: 4,
      numOfElements: 0,
      currentFilter: "",
      searchBy: "",
    };
  }

  check = () => {
    if (this.state.page >= 0) {
      this.getElementsByPageAndSize(
        this.state.page,
        this.state.size,
        this.props.filter,
        this.state.searchBy
      );
    }
  };

  handlePageNext = (e) => {
    e.preventDefault();

    this.setState((prevState) => {
      return {
        page: prevState.page + 1,
      };
    }, this.check);
  };

  handlePagePrev = (e) => {
    e.preventDefault();

    if (this.state.page > 0) {
      this.setState((prevState) => {
        return {
          page: prevState.page - 1,
        };
      }, this.check);
    }
  };

  getElementsByPageAndSize = (page, size, filter, search) => {
    this.setState({ loading: true });
    let url = ``;
    if (filter == elementConstants.SHOW_BY_LOCATION) {
      url = `acs/elements/2020b.ben.halfon/${this.props.user.email}/search/near/${userLocation.lat}/${userLocation.lng}/${userLocation.distance}?page=${page}&size=${size}`;
    } else if (filter == elementConstants.SHOW_BY_NAME) {
      this.setState({ searchBy: search });
      url = `acs/elements/2020b.ben.halfon/${this.props.user.email}/search/byName/${search}?page=${page}&size=${size}`;
    } else if (filter == elementConstants.SHOW_BY_TYPE) {
      this.setState({ searchBy: search });
      url = `acs/elements/2020b.ben.halfon/${this.props.user.email}/search/byType/${search}?page=${page}&size=${size}`;
    } else {
      url = `acs/elements/2020b.ben.halfon/${this.props.user.email}?page=${page}&size=${size}`;
    }
    axios
      .get(url)
      .then((response) => {
        if (response.status >= 200 && response.status < 300) {
          let arr = [];
          response.data.forEach((element) => {
            arr.push(elementBoundary(element));
          });
          this.setState({
            elements: arr,
            loading: false,
            numOfElements: arr.length,
          });
        } else {
          const error = new Error(response.statusText);
          error.response = response;
          throw error;
        }
      })
      .catch((error) => {
        console.log("request failed", error);
      });
  };

  componentDidMount() {
    this.getElementsByPageAndSize(
      this.state.page,
      this.state.size,
      elementConstants.SHOW_BY_LOCATION
    );
  }

  destructLocation = (location) => {
    let arr = location.split(",");
    let newLocation = {
      lat: arr[0],
      lng: arr[1],
    };

    return newLocation;
  };

  updateElement = (elementData, userData) => {
    axios
      .put(
        `/acs/elements/${userData.domain}/${userData.email}/${elementData.domain}/${elementData.id}`,
        {
          name: elementData.name,
          type: elementData.type,
          elementAttributes: elementData.elementAttributes,
          active: elementData.active,
          location: this.destructLocation(elementData.location),
        }
      )
      .then((response) => {
        if (response.status >= 200 && response.status < 300) {
          // dispatch(addElementSuccess(response.data));
          this.getElementsByPageAndSize(
            this.state.page,
            this.state.size,
            this.props.filter,
            this.props.search
          );
        } else {
          const error = new Error(response.statusText);
          error.response = response;
          // dispatch(addElementFailure(error));
          throw error;
        }
      })
      .catch((error) => {
        console.log("request failed", error);
      });
  };

  addElement = (elementData, userData) => {
    axios
      .post(`/acs/elements/${userData.domain}/${userData.email}`, {
        name: elementData.name,
        type: elementData.type,
        elementAttributes: elementData.elementAttributes,
        active: elementData.active,
        location: this.destructLocation(elementData.location),
      })
      .then((response) => {
        if (response.status >= 200 && response.status < 300) {
          this.getElementsByPageAndSize(
            0,
            this.state.size,
            elementConstants.SHOW_BY_LOCATION,
            ""
          );
        } else {
          const error = new Error(response.statusText);
          error.response = response;
          throw error;
        }
      })
      .catch((error) => {
        console.log("request failed", error);
      });
  };

  clickHere = () => {
    let filter = this.props.filter;
    if (
      this.props.filter === elementConstants.SHOW_BY_NAME ||
      this.props.filter === elementConstants.SHOW_BY_TYPE
    ) {
      filter = elementConstants.SHOW_ALL;
    }
    this.getElementsByPageAndSize(0, this.state.size, filter, "");

    this.setState({ page: 0 });
  };

  actions = (elementData, userData) => {
    const element = {
      ...elementData,
      invokedBy: {
        userId: {
          domain: userData.domain,
          email: userData.email,
        },
      },
    };
    axios
      .post(`/acs/actions`, {
        ...element,
      })
      .then((response) => {
        if (response.status >= 200 && response.status < 300) {
          // dispatch(addElementSuccess(response.data));
          this.getElementsByPageAndSize(
            this.state.page,
            this.state.size,
            this.props.filter,
            this.props.search
          );
        } else {
          const error = new Error(response.statusText);
          error.response = response;
          // dispatch(addElementFailure(error));
          throw error;
        }
      })
      .catch((error) => {
        console.log("request failed", error);
      });
  };

  render() {
    return (
      <div className="elements-container">
        <div>
          <Provider
            value={{
              elements: this.state.elements,
              next: this.handlePageNext,
              previous: this.handlePagePrev,
              getElements: this.getElementsByPageAndSize,
              updateElement: this.updateElement,
              addElement: this.addElement,
              actions: this.actions,
              size: this.state.size,
            }}
          >
            {this.state.loading ? (
              <div class="spinner-border text-light spinner-pos" role="status">
                <span class="sr-only">Loading...</span>
              </div>
            ) : this.state.numOfElements == 0 ? (
              <div className="elements-appear">
                Oops! No more elements... Click{" "}
                <span className="here-click" onClick={this.clickHere}>
                  {" "}
                  Here{" "}
                </span>{" "}
                to go back
              </div>
            ) : (
              <Options />
            )}
          </Provider>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: {
      role: state.users.user.role,
      email: state.users.user.email,
    },
    filter: state.elementVisibility.visibilityFilter,
    search: state.elementVisibility.search,
  };
}

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Elements);
