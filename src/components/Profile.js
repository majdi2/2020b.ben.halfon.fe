// @flow
import React, { Component } from "react";
import "./profile.css";
import { connect } from "react-redux";
import "../boundaries/userBoundary";

import Avatar from "@material-ui/core/Avatar";
import angrySmiley from "../resources/images/iconfinder_angry_1325170.png";
import robot from "../resources/images/robot.png";
import ghost from "../resources/images/ghost.png";
import ugly from "../resources/images/ugly.png";
import EditIcon from "@material-ui/icons/Edit";
import CloseIcon from "@material-ui/icons/Close";
import { Dropdown, DropdownButton } from "react-bootstrap";
import { update } from "../actions/userActions";

class Profile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      editProfile: false,
      email: this.props.user.email,
      username: this.props.user.username,
      avatar: this.props.user.avatar,
      role: this.props.user.role,
      checked: this.props.user.role === "MANAGER" ? false : true,
    };
  }

  editProfile = () => {
    this.setState({
      editProfile: !this.state.editProfile,
      username: this.props.user.username,
      avatar: this.props.user.avatar,
      role: this.props.user.role,
      checked: this.props.user.role === "MANAGER" ? false : true,
    });
  };

  handleName = (e) => {
    this.setState({
      username: e.target.value,
    });
  };

  handleRole = (e) => {
    this.setState({
      role: e.target.value,
      checked: !this.state.checked,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();

    const userData = {
      domain: this.props.user.domain,
      email: this.state.email,
      username: this.state.username,
      avatar: this.state.avatar,
      role: this.state.role,
      existingUser: {
        domain: this.props.user.domain,
        email: this.props.user.email,
      },
    };
    this.setState({
      editProfile: false,
    });
    this.props.dispatch(update(userData));
  };

  showCorrectType = (type) => {
    switch (type) {
      case "robot":
        return <Avatar src={robot} />;
      case "ghost":
        return <Avatar src={ghost} />;
      case "ugly":
        return <Avatar src={ugly} />;
      case "angry":
        return <Avatar src={angrySmiley} />;
    }
  };

  render() {
    return (
      <div className="profile-container">
        <div className="profile-col">
          <div className="edit-profile">
            <div className="user-avatar">
              <div className="avatar">
                {this.showCorrectType(this.props.user.avatar)}
              </div>

              <h1>{this.props.user.username}'s Profile</h1>
              {/* <div className="profile-row">{this.props.user.userId.email}</div> */}
            </div>

            <div className="icon-container">
              {!this.state.editProfile && (
                <EditIcon type="button" onClick={this.editProfile}></EditIcon>
              )}

              {this.state.editProfile && (
                <CloseIcon type="button" onClick={this.editProfile}></CloseIcon>
              )}
            </div>
          </div>
          <div className="profile-form">
            <form onSubmit={this.handleSubmit}>
              <div className="profile-row">
                <label style={{ fontSize: "25px" }}>Username: </label>
                {this.state.editProfile && (
                  <div className="edit-row">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Type new username"
                      value={this.state.username}
                      onChange={this.handleName}
                    />
                    {/* </form> */}
                  </div>
                )}
                {!this.state.editProfile && (
                  <div className="edit-row">
                    <label> {this.state.username} </label>
                  </div>
                )}
              </div>
              <div className="profile-row">
                {/* <form> */}
                <label style={{ fontSize: "25px" }}>Role: </label>
                {this.state.editProfile && (
                  <div className="edit-row">
                    <div>
                      <input
                        type="radio"
                        value="MANAGER"
                        onChange={this.handleRole}
                        checked={!this.state.checked}
                      />
                      <label> Manager </label>
                    </div>
                    <div>
                      <input
                        type="radio"
                        value="PLAYER"
                        onChange={this.handleRole}
                        checked={this.state.checked}
                      />
                      <label> Player </label>
                    </div>
                  </div>
                )}
                {!this.state.editProfile && (
                  <div className="edit-row">
                    <label> {this.state.role} </label>
                  </div>
                )}
              </div>
              {this.state.editProfile && (
                <div className="profile-row">
                  <label style={{ fontSize: "25px" }}>Avatar: </label>
                  <div className="edit-row">
                    {/* <form> */}
                    <div className="avatar-container">
                      <DropdownButton
                        id="dropdown-basic-button"
                        title="Select Avatar"
                      >
                        <Dropdown.Item
                          onClick={() => this.setState({ avatar: "robot" })}
                        >
                          <Avatar src={robot} />
                        </Dropdown.Item>
                        <Dropdown.Item
                          onClick={() => this.setState({ avatar: "angry" })}
                        >
                          <Avatar src={angrySmiley} />
                        </Dropdown.Item>
                        <Dropdown.Item
                          onClick={() => this.setState({ avatar: "ghost" })}
                        >
                          <Avatar src={ghost} />
                        </Dropdown.Item>
                        <Dropdown.Item
                          onClick={() => this.setState({ avatar: "ugly" })}
                        >
                          <Avatar src={ugly} />
                        </Dropdown.Item>
                      </DropdownButton>
                    </div>
                    {/* </form> */}
                  </div>
                </div>
              )}

              <div className="profile-row">
                <div className="edit-row">
                  {/* <form> */}
                  <label style={{ fontSize: "25px" }}>Email: </label>
                </div>

                <div className="edit-row">
                  <label> {this.state.email} </label>
                </div>
              </div>
              {this.state.editProfile && (
                <div className="submit-btn">
                  <button type="submit" className="btn btn-primary btn-block">
                    Update Profile
                  </button>
                </div>
              )}
            </form>
          </div>
        </div>
      </div>
    );
  }
}

function mapStatetoProps(state) {
  return {
    user: state.users.user,
  };
}
export default connect(mapStatetoProps, null)(Profile);
