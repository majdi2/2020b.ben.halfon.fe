import axios from "../helpers/axiosHelper";
import { elementConstants } from "../constants/elementConstants";

// export const elementsServices = {
//   elementsByName,
//   getElementsByPageAndSize,
// };

export const getElementsByPageAndSizeService = (
  userData,
  filter,
  page,
  size
) => {
  let url = ``;
  if (filter == elementConstants.SHOW_BY_LOCATION) {
    url = `acs/elements/2020b.ben.halfon/${userData.email}/search/near/15/5/26?page=${page}&size=${size}`;
  } else if (filter == elementConstants.SHOW_ALL) {
    url = `acs/elements/2020b.ben.halfon/${userData.email}?page=${page}&size=${size}`;
  }
  let elements = [];
  axios
    .get(url)
    .then((response) => {
      if (response.status >= 200 && response.status < 300) {
        // this.setState({

        elements = response.data;
        // });
      } else {
        const error = new Error(response.statusText);
        error.response = response;
        throw error;
      }
    })
    .catch((error) => {
      console.log("request failed", error);
    });
  return elements;
};

function elementsByName(userData, name) {
  const elements = [];
  axios
    .get(
      `/acs/elements/${userData.domain}/${userData.email}/search/byName/${name}`
    )
    .then((response) => {
      if (response.status >= 200 && response.status < 300) {
      } else {
        const error = new Error(response.statusText);
        error.response = response;
        throw error;
      }
    })
    .catch((error) => {
      console.log("request failed", error);
    });
}
