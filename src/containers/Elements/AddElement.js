// @flow
import React, { Component, useState } from "react";
import { connect } from "react-redux";
// import Backdrop from "../../components/backdrop/Backdrop";
import { addElement } from "../../actions/elementsActions";
// import Modal from "../../components/modal/modal";
import { Button, Dropdown, DropdownButton } from "react-bootstrap";
import ModalElement from "./Modal";
import LoadingContext from "../../context/elementsContext";
class AddElement extends Component {
  static contextType = LoadingContext;

  constructor(props) {
    super(props);

    this.state = {
      show: false,
      name: "",
      type: "",
      location: {
        lat: 0,
        lng: 0,
      },
      description: "",
    };
  }

  handleShow = () => {
    this.setState({ show: true });
  };

  saveChanges = (el) => {
    this.addElement(el);
    this.setState({ show: false });
  };

  addElement = (el) => {
    const user = {
      domain: this.props.user.domain,
      email: this.props.user.email,
    };
    this.context.addElement(el, user);
    // this.context.getElements(0, 4, this.props.filter);
  };

  handleClose = () => {
    this.setState({ show: false });
  };

  render() {
    return (
      <>
        <Button onClick={this.handleShow} variant="light">
          Add Dirty Location
        </Button>

        <ModalElement
          title="Add Dirty Location"
          active={true}
          editMode={false}
          show={this.state.show}
          close={this.handleClose}
          save={this.saveChanges}
        />
      </>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: {
      role: state.users.user.role,
      email: state.users.user.email,
      domain: state.users.user.domain,
    },
  };
}

const mapDispatchToProps = (dispatch) => ({
  addElement: (el, user) => dispatch(addElement(el, user)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddElement);
