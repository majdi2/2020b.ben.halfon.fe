import React, { Component, createRef, useState } from "react";
import { connect } from "react-redux";
import AddElement from "./AddElement";
import DeleteIcon from "@material-ui/icons/Delete";
import ClearIcon from "@material-ui/icons/Clear";
import WavesIcon from "@material-ui/icons/Waves";
import EcoIcon from "@material-ui/icons/Eco";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import { Menu, MenuItem } from "@material-ui/core";
import { Consumer } from "../../context/elementsContext";
import LoadingContext from "../../context/elementsContext";
import Rating from "@material-ui/lab/Rating";
import Box from "@material-ui/core/Box";
import ReportsModal from "./ReportsModal";
import ModalElement from "./Modal";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
class FilterElements extends Component {
  static contextType = LoadingContext;

  constructor(props, context) {
    super(props, context);

    this.state = {
      show: false,
      anchorEl: null,
      loading: true,
      update: false,
      selectedIndex: "",
      edit: false,
      addRatings: false,
      showRatings: false,
    };
  }

  handleShow = () => {
    this.setState({ show: true });
  };

  showCorrectType = (type) => {
    switch (type) {
      case "trash_location":
        return (
          <OverlayTrigger
            placement="top"
            overlay={<Tooltip id={`tooltip-top`}>Trash</Tooltip>}
          >
            <DeleteIcon />
          </OverlayTrigger>
        );
      case "sea_location":
        return (
          <OverlayTrigger
            placement="top"
            overlay={<Tooltip id={`tooltip-top`}>Sea</Tooltip>}
          >
            <WavesIcon />
          </OverlayTrigger>
        );

      case "park_location":
        return (
          <OverlayTrigger
            placement="top"
            overlay={<Tooltip id={`tooltip-top`}>Park</Tooltip>}
          >
            <EcoIcon />
          </OverlayTrigger>
        );

      default:
        return (
          <OverlayTrigger
            placement="top"
            overlay={<Tooltip id={`tooltip-top`}>No Type</Tooltip>}
          >
            <ClearIcon />
          </OverlayTrigger>
        );
    }
  };

  handleClose = (e) => {
    this.setState({ anchorEl: null });
  };

  handleClick = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  };

  calcRating = (ratings) => {
    if (ratings) {
      let ratingVal = 0;
      ratings.forEach((element) => {
        ratingVal += element.trashLevel;
      });
      return ratingVal / ratings.length;
    }
    return 0;
  };

  handlerUpdateModal = (e) => {
    this.handleClose(e);
    this.setState({
      show: true,
      selectedIndex: this.state.anchorEl.dataset.index,
      edit: true,
    });
  };

  handleCloseModal = () => {
    this.setState({ show: false, showRatings: false, edit: false });
  };

  updateElementFromChild = (el) => {
    this.handleCloseModal();
    const user = {
      domain: this.props.user.domain,
      email: this.props.user.email,
    };

    el = {
      ...el,
      id: this.context.elements[this.state.selectedIndex].id,
      domain: this.context.elements[this.state.selectedIndex].domain,
    };
    this.context.updateElement(el, user);
  };

  addReportFromChildPlayerWithAction = (el) => {
    this.handleCloseModal();
    const user = {
      domain: this.props.user.domain,
      email: this.props.user.email,
    };

    el = {
      element: {
        elementId: {
          id: this.context.elements[this.state.selectedIndex].id,
          domain: this.context.elements[this.state.selectedIndex].domain,
        },
      },
      type: "addReport",
      actionAttributes: {
        report: el.actionAttributes.report,
      },
    };
    this.context.actions(el, user);
  };

  addReport = (e) => {
    this.handleClose(e);
    this.setState({
      showRatings: true,
    });
    this.setState({ selectedIndex: this.state.anchorEl.dataset.index });
    this.setState({ addRatings: true });
  };

  cleanReports = (e) => {
    this.handleClose(e);
    let el = {};
    let user = {};

    user = {
      domain: this.props.user.domain,
      email: this.props.user.email,
    };

    el = {
      element: {
        elementId: {
          id: this.context.elements[this.state.anchorEl.dataset.index].id,
          domain: this.context.elements[this.state.anchorEl.dataset.index]
            .domain,
        },
      },
      type: "cleanReports",
    };
    this.context.actions(el, user);
  };

  render() {
    return (
      <Consumer>
        {(context) => (
          <React.Fragment>
            <div className="elements-container">
              <div id="map-container">
                <ul>
                  {context.elements.map((el, inx) => {
                    return (
                      <li
                        data-index={inx}
                        className="elements-space elements-appear"
                        key={inx}
                      >
                        <div className="icon-container">
                          <div>{this.showCorrectType(el.type)}</div>
                          {this.props.user.role === "MANAGER" ? (
                            <div className="active-element">
                              {el.active ? (
                                <span style={{ color: "green" }}>Active</span>
                              ) : (
                                <span style={{ color: "red" }}>Inactive</span>
                              )}
                            </div>
                          ) : null}
                        </div>
                        <div className="content">
                          <h5 className="name">{el.name}</h5>

                          <div className="description">
                            {!(
                              el.elementAttributes.description.trim() == ""
                            ) ? (
                              el.elementAttributes.description
                            ) : (
                              <div>
                                {this.props.user.role === "MANAGER" ? (
                                  <div>Add description...</div>
                                ) : null}
                              </div>
                            )}
                          </div>
                        </div>
                        <div className="arrow-rating">
                          <Box
                            component="fieldset"
                            mb={3}
                            borderColor="transparent"
                          >
                            <Rating
                              name="read-only"
                              value={
                                // el.elementAttributes.reports ?
                                this.calcRating(el.elementAttributes.reports)
                                // : 0
                              }
                              readOnly
                              defaultValue={1}
                              size="small"
                            />
                          </Box>
                          <div className="arrow">
                            <ArrowForwardIosIcon
                              data-index={inx}
                              onClick={this.handleClick}
                            />
                            {this.props.user.role === "PLAYER" && (
                              <Menu
                                id="simple-menu"
                                anchorEl={this.state.anchorEl}
                                keepMounted
                                open={this.state.anchorEl ? true : false}
                                onClose={this.handleClose}
                              >
                                <MenuItem onClick={this.addReport}>
                                  Rating
                                </MenuItem>
                                <MenuItem onClick={this.cleanReports}>
                                  Cleaned
                                </MenuItem>
                              </Menu>
                            )}

                            {this.props.user.role === "MANAGER" && (
                              <Menu
                                id="simple-menu"
                                anchorEl={this.state.anchorEl}
                                keepMounted
                                open={this.state.anchorEl ? true : false}
                                onClose={this.handleClose}
                              >
                                <MenuItem onClick={this.handlerUpdateModal}>
                                  Update
                                </MenuItem>
                              </Menu>
                            )}
                          </div>
                        </div>
                      </li>
                    );
                  })}
                </ul>

                {this.state.addRatings && (
                  <ReportsModal
                    show={this.state.showRatings}
                    close={this.handleCloseModal}
                    save={this.addReportFromChildPlayerWithAction}
                    element={context.elements[this.state.selectedIndex]}
                  />
                )}

                {this.state.edit && (
                  <ModalElement
                    name={this.context.elements[this.state.selectedIndex].name}
                    active={
                      this.context.elements[this.state.selectedIndex].active
                    }
                    type={this.context.elements[this.state.selectedIndex].type}
                    description={
                      this.context.elements[this.state.selectedIndex]
                        .elementAttributes.description
                    }
                    location={
                      this.context.elements[this.state.selectedIndex].location
                    }
                    title={
                      "Update Element - " +
                      this.context.elements[this.state.selectedIndex].name
                    }
                    editMode={true}
                    show={this.state.show}
                    close={this.handleCloseModal}
                    save={this.updateElementFromChild}
                  />
                )}
                <div className="next-prev">
                  <ul className="links">
                    <li className="link" onClick={context.previous}>
                      prev
                    </li>
                    <li className="link" onClick={context.next}>
                      next
                    </li>
                  </ul>
                  <div className="add-element">
                    {this.props.user.role == "MANAGER" ? <AddElement /> : null}
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        )}
      </Consumer>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: {
      role: state.users.user.role,
      email: state.users.user.email,
      domain: state.users.user.domain,
    },
    filter: state.elementVisibility.visibilityFilter,
    search: state.elementVisibility.search,
  };
}

export default connect(mapStateToProps, null)(FilterElements);
