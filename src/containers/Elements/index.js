import React, { Component } from "react";

import FilterElements from "./FilterElements";
import { connect } from "react-redux";
import { setVisibilityFilter } from "../../actions/elementsActions";
import { elementConstants } from "../../constants/elementConstants";
import FilterListIcon from "@material-ui/icons/FilterList";
import { Menu, MenuItem } from "@material-ui/core";
import LoadingContext from "../../context/elementsContext";
import {
  Modal,
  Button,
  Dropdown,
  DropdownButton,
  SplitButton,
} from "react-bootstrap";
import "./index.css";
import { Consumer } from "../../context/elementsContext";
class ElementsOptions extends Component {
  static contextType = LoadingContext;

  constructor(props) {
    super(props);

    this.state = {
      anchorEl: null,
      searchBy: "",
      search: "",
      type: "",
    };
  }

  showByName = () => {
    this.props.setVisibilityFilter(
      elementConstants.SHOW_BY_NAME,
      this.state.search
    );
    this.context.getElements(
      0,
      this.context.size,
      elementConstants.SHOW_BY_NAME,
      this.state.search
    );
  };

  showByType = () => {
    this.props.setVisibilityFilter(
      elementConstants.SHOW_BY_TYPE,
      this.state.type
    );
    this.context.getElements(
      0,
      this.context.size,
      elementConstants.SHOW_BY_TYPE,
      this.state.type
    );
  };

  handleSearch = (e) => {
    this.setState({ search: e.target.value });
  };

  handleClose = (e) => {
    this.setState({ anchorEl: null });
  };

  handlerUpdate = (e) => {
    this.handleClose(e);
  };

  handleClick = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  };
  handleShowLocationElements = () => {
    this.handleClose();
    this.props.setVisibilityFilter(elementConstants.SHOW_BY_LOCATION, "");
    this.context.getElements(
      0,
      this.context.size,
      elementConstants.SHOW_BY_LOCATION
    );
  };

  handleShowAllElements = () => {
    this.handleClose();
    this.props.setVisibilityFilter(elementConstants.SHOW_ALL, "");
    this.context.getElements(0, this.context.size, elementConstants.SHOW_ALL);
  };

  render() {
    return (
      <React.Fragment>
        <div className="buttons-container">
          <div className="title-div">
            <h1 className="title">Dirty Locations</h1>
          </div>

          <div className="search-filter">
            <div className="filter-icon">
              <FilterListIcon color="white" onClick={this.handleClick} />
              <Menu
                id="simple-menu"
                anchorEl={this.state.anchorEl}
                keepMounted
                open={this.state.anchorEl ? true : false}
                onClose={this.handleClose}
              >
                <MenuItem onClick={this.handleShowLocationElements}>
                  Show By Location
                </MenuItem>
                <MenuItem onClick={this.handleShowAllElements}>
                  Show All
                </MenuItem>
              </Menu>
            </div>
            <div className="spaceing">
              <DropdownButton
                id="dropdown-basic-button"
                title="Search By Type"
                className="drop-btn"
              >
                <Dropdown.Item
                  onClick={() => {
                    this.setState({ type: "trash_location" }, () =>
                      this.showByType()
                    );
                  }}
                >
                  Trash
                </Dropdown.Item>
                <Dropdown.Item
                  onClick={() => {
                    this.setState({ type: "sea_location" }, () =>
                      this.showByType()
                    );
                  }}
                >
                  Sea
                </Dropdown.Item>
                <Dropdown.Item
                  onClick={() => {
                    this.setState({ type: "park_location" }, () =>
                      this.showByType()
                    );
                  }}
                >
                  Park
                </Dropdown.Item>
              </DropdownButton>
            </div>

            <form className="form-inline my-2 my-lg-0">
              <input
                className="form-control mr-sm-2"
                type="search"
                placeholder="Search"
                aria-label="Search"
                onChange={this.handleSearch}
                value={this.state.search}
              />
              <button
                type="button"
                class="btn btn-light"
                onClick={this.showByName}
              >
                Search
              </button>
            </form>
          </div>
        </div>

        <FilterElements />
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: {
      role: state.users.user.role,
      email: state.users.user.email,
    },
  };
}

const mapDispatchToProps = (dispatch) => ({
  setVisibilityFilter: (filter, search) =>
    dispatch(setVisibilityFilter(filter, search)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ElementsOptions);
