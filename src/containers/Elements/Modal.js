// @flow
import React, { Component } from "react";
import { Modal, Button, Dropdown, DropdownButton } from "react-bootstrap";
import ReportsModal from "./ReportsModal";
class ModalElement extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: this.props.name,
      type: this.props.type,
      location: this.props.location,
      description: this.props.description,
      location: this.props.location,
      active: this.props.active,
    };
  }

  handleLocation = (e) => {
    this.setState({ location: e.target.value });
  };

  toggleActive = () => {
    this.setState({ active: !this.state.active });
  };

  handleName = (e) => {
    this.setState({ name: e.target.value });
  };

  handleDescription = (e) => {
    this.setState({ description: e.target.value });
  };

  handleCloseDeleteAll = () => {
    this.setState({
      name: "",
      type: "",
      location: "",
      description: "",
      location: "",
      active: "",
    });

    this.props.close();
  };

  handleClose = () => {
    this.props.close();
  };

  saveChanges = () => {
    const el = {
      name: this.state.name,
      type: this.state.type,
      elementAttributes: {
        description: this.state.description,
      },
      location: this.state.location,
      active: this.state.active,
    };

    // this.setState({
    //   name: "",
    //   type: "",
    //   location: "",
    //   description: "",
    //   location: "",
    //   active: "",
    // });

    this.props.save(el);
  };

  componentDidMount() {
    // this.setState({
    //   name: this.props.name,
    //   type: this.props.type,
    //   location: this.props.location,
    //   description: this.props.description,
    //   location: this.props.location,
    //   active: this.props.active,
    // });
  }

  render() {
    return (
      <>
        <Modal show={this.props.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>{this.props.title}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="form-group">
              <label>Name</label>
              <input
                type="text"
                className="form-control"
                placeholder="Enter Name"
                onChange={this.handleName}
                value={this.state.name}
              />
            </div>

            <div className="form-group">
              <label>Location</label>
              <input
                type="text"
                className="form-control"
                placeholder="Enter lat and lng seperated with comma. Example: 12, 1"
                onChange={this.handleLocation}
                value={this.state.location}
              />
            </div>

            <div className="form-group">
              <label>Description</label>

              <textarea
                rows="4"
                cols="50"
                className="form-control"
                onChange={this.handleDescription}
                value={this.state.description}
              />
            </div>

            {/* need to add here a 'find my location option' and populate it in state when clicked */}

            <div className="spaceing">
              <DropdownButton id="dropdown-basic-button" title="Select Type">
                <Dropdown.Item
                  onClick={() => this.setState({ type: "trash_location" })}
                >
                  Trash
                </Dropdown.Item>
                <Dropdown.Item
                  onClick={() => this.setState({ type: "sea_location" })}
                >
                  Sea
                </Dropdown.Item>
                <Dropdown.Item
                  onClick={() => this.setState({ type: "park_location" })}
                >
                  Park
                </Dropdown.Item>
              </DropdownButton>
            </div>
            <div
              style={{ width: "5%", marginTop: "15px" }}
              onClick={this.toggleActive}
            >
              {this.state.active ? (
                <span style={{ color: "green", cursor: "pointer" }}>
                  Active
                </span>
              ) : (
                <span style={{ color: "red", cursor: "pointer" }}>
                  Inactive
                </span>
              )}
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Cancel
            </Button>
            <Button variant="primary" onClick={this.saveChanges}>
              Save Element
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}

export default ModalElement;
