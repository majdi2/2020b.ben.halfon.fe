// @flow
import React, { Component } from "react";
import { Modal, Button } from "react-bootstrap";
import Rating from "@material-ui/lab/Rating";
import Box from "@material-ui/core/Box";
class ReportsModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      report: 0,
    };
  }

  handleClose = () => {
    this.props.close();
  };

  saveChanges = () => {
    const el = {
      actionAttributes: {
        report: {
          trashLevel: parseInt(this.state.report, 10),
          comment: "",
        },
      },
    };
    this.props.save(el);
  };

  getReport = (e) => {
    this.setState({ report: e.target.value });
  };

  render() {
    return (
      <>
        <Modal show={this.props.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>How Dirty Is It? (5 Star means clean)</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Box component="fieldset" mb={3} borderColor="transparent">
              <Rating
                name="read-only"
                value={this.state.report}
                readOnly={false}
                defaultValue={0}
                size="small"
                onChange={this.getReport}
              />
            </Box>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Cancel
            </Button>
            <Button variant="primary" onClick={this.saveChanges}>
              Save Ratings
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}

export default ReportsModal;
