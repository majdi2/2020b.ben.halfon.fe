import React from "react";
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import { elementConstants } from "../constants/elementConstants";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
  NavLink,
} from "react-router-dom";
import { connect } from "react-redux";
import SignUp from "../components/Signup";
import Login from "../components/Login";
import Elements from "../components/Elements";
import { logout, login } from "../actions/userActions";
import "./index.css";
import NotMatched from "../components/NotMatched";
import { setVisibilityFilter } from "../actions/elementsActions";
import Profile from "../components/Profile";

class MainNavigation extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showCollapsedMenu: true,
      search: "",
      searchBy: "",
    };
  }

  toggleMenu = () => {
    this.setState({
      showCollapsedMenu: !this.state.showCollapsedMenu,
    });
  };

  componentDidMount() {
    const isLogged = localStorage.getItem("token");

    if (isLogged) {
      const email = localStorage.getItem("email");
      const userData = {
        email,
        domain: "2020b.ben.halfon",
      };
      this.props.login(userData);
    }
  }

  render() {
    const show = this.state.showCollapsedMenu ? "show" : "";
    return (
      <Router>
        <div className="pos-f-t">
          <nav className="navbar navbar-light bg-light navbar-default navbar-fixed-top">
            <div className="screen-size">
              <NavLink className="nav-link" to={"/"}>
                <div class="navbar-brand">iClean</div>
              </NavLink>
              <button
                className="navbar-toggler"
                type="button"
                onClick={this.toggleMenu} // <=
                data-toggle="collapse"
                data-target="#navbarToggleExternalContent"
              >
                <span className="navbar-toggler-icon"></span>
              </button>
            </div>
          </nav>
          <div
            className={"collapse flex-row navbar-collapse collapse " + show}
            id="navbarToggleExternalContent"
          >
            <ul className="coll-nav options-bar">
              {!this.props.isLoggedIn && (
                <>
                  <li className="nav-item">
                    <NavLink className="nav-link" to={"/"}>
                      Login
                    </NavLink>
                  </li>

                  <li className="nav-item">
                    <NavLink className="nav-link" to={"/sign-up"}>
                      Sign up
                    </NavLink>
                  </li>
                </>
              )}
              {this.props.isLoggedIn && (
                <React.Fragment>
                  <li className="nav-item">
                    <NavLink className="nav-link" to={"/profile"}>
                      Profile
                    </NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link" to={"/elements"}>
                      Locations
                    </NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink
                      className="nav-link"
                      to={"/"}
                      onClick={() => this.props.logout()}
                    >
                      Logout
                    </NavLink>
                  </li>
                  <li></li>
                </React.Fragment>
              )}
            </ul>
          </div>

          {this.props.isLoggedIn && (
            <div>
              <Switch>
                <Route path="/elements" component={Elements} />
                <Route path="/profile" component={Profile} />

                <Redirect from="/" to="/elements" exact />

                <Redirect from="/sign-up" to="/elements" exact />

                <Route path="*" component={NotMatched} />
              </Switch>
            </div>
          )}

          {!this.props.isLoggedIn && (
            <div className="auth-wrapper">
              <div className="auth-inner">
                <Switch>
                  <Route path="/sign-up" component={SignUp} />

                  <Route exact path="/" component={Login} />
                  <Redirect from="/profile" to="/" exact />
                  <Redirect from="/elements" to="/" exact />

                  <Route path="*" component={NotMatched} />
                </Switch>
              </div>
            </div>
          )}
        </div>
      </Router>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoggedIn: state.users.isLoggedIn,
    role: state.users.user.role,
    filter: state.elementVisibility.visibilityFilter,
  };
}

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(logout()),
  login: (user) => dispatch(login(user)),
  setVisibilityFilter: (filter, search) =>
    dispatch(setVisibilityFilter(filter, search)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MainNavigation);
