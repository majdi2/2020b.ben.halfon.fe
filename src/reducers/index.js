import { combineReducers } from "redux";
import { userConstants } from "../constants/userConstants";
// import { authentication } from "./authentication";
// import { registration } from "./registration";
import { users } from "./usersReducer";
// import elements from "./elements/elements";
import elementVisibility from "./elements/elementsVisibilty";
// import elementReducer from "./elements";
import { elements } from "./elements/elements";

const appReducer = combineReducers({
  users,
  elements,
  elementVisibility,
});
const rootReducer = (state, action) => {
  if (action.type == userConstants.LOGOUT) {
    localStorage.clear();
    state = undefined;
  }
  return appReducer(state, action);
};

export default rootReducer;
