import { elementConstants } from "../../constants/elementConstants";

const elementVisibility = (
  state = { visibilityFilter: elementConstants.SHOW_BY_LOCATION, search: "" },
  action
) => {
  switch (action.type) {
    case "SET_VISIBILITY_FILTER":
      return Object.assign({}, state, {
        visibilityFilter: action.filter,
        search: action.search,
      });
    default:
      return state;
  }
};

export default elementVisibility;
