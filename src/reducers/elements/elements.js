import { elementConstants } from "../../constants/elementConstants";

export const elements = (state = [], action) => {
  switch (action.type) {
    case elementConstants.UPDATE_ELEMENT:
      return Object.assign({}, state, {
        updateInx: parseInt(action.index, 10),
      });
    default:
      return state;
  }
};
