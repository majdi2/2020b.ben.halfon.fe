import { combineReducers } from "redux";
import { elements } from "./elements";
import elementVisibility from "./elementsVisibilty";

const elementsReducer = combineReducers({
  elements,

  elementVisibility,
});

export default elementsReducer;
