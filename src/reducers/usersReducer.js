import { userConstants } from "../constants/userConstants";

export function users(state = { isLoggedIn: false, user: {} }, action) {
  switch (action.type) {
    case userConstants.SIGN_UP_REQUEST:
      return Object.assign({}, state, {
        isLoggedIn: false,
      });
    case userConstants.SIGN_UP:
      return Object.assign({}, state, {
        isLoggedIn: true,
        user: action.user,
      });
    case userConstants.SIGN_UP_FAILED:
      return Object.assign({}, state, {
        isLoggedIn: false,
        user: {},
      });
    case userConstants.LOGIN_REQUEST:
      return Object.assign({}, state, {
        isLoggedIn: false,
      });
    case userConstants.LOGIN_SUCCESS:
      return Object.assign({}, state, {
        isLoggedIn: true,
        user: action.user,
      });
      case userConstants.UPDATE:
        return Object.assign({}, state, {
          isLoggedIn: true,
          user: action.user,
        });
    case userConstants.LOGIN_FAILURE:
      return {};
    default:
      return state;
  }
}
