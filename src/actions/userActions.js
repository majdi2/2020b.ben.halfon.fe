import { userConstants } from "../constants/userConstants";
import axios from "../helpers/axiosHelper";
import { userBoundary } from "../boundaries/userBoundary";

export function loginError(error) {
  return { error, type: userConstants.LOGIN_FAILURE };
}

export function loginSuccess(response) {
  return (dispatch) => {
    const user = dispatch({
      user: userBoundary(response),
      type: userConstants.LOGIN_SUCCESS,
    });
  };
}

export function loginRequest(email) {
  const user = { email: email };
  return { user, type: userConstants.LOGIN_REQUEST };
}

export function logout() {
  return { type: userConstants.LOGOUT };
}

export function login(userData) {
  return (dispatch, getState) => {
    dispatch(loginRequest(userData.email));
    axios
      .get(`/acs/users/login/${userData.domain}/${userData.email}`)
      .then((response) => {
        if (response.status >= 200 && response.status < 300) {
          localStorage.setItem("token", true);
          localStorage.setItem("email", userData.email);
          dispatch(loginSuccess(response.data));
        } else {
          const error = new Error(response.statusText);
          error.response = response;
          dispatch(loginError(error));
          throw error;
        }
      })
      .catch((error) => {
      });
  };
}

export function signupSuccess(response) {
  return (dispatch) => {
    const user = dispatch({
      user: userBoundary(response),
      type: userConstants.SIGN_UP,
    });
  };
}

export function signupFailed(error) {
  return { error, type: userConstants.SIGN_UP_FAILED };
}

export function signupRequest(email, username, role, avatar) {
  const user = { email, username, role, avatar };
  return { user, type: userConstants.SIGN_UP_REQUEST };
}

export function signup(userData) {
  return (dispatch, getState) => {
    dispatch(
      signupRequest(
        userData.email,
        userData.username,
        userData.role,
        userData.avatar
      )
    );
    axios
      .post("/acs/users", {
        username: userData.username,
        email: userData.email,
        role: userData.role,
        avatar: userData.avatar,
      })
      .then((response) => {
        if (response.status >= 200 && response.status < 300) {
          dispatch(signupSuccess(response.data));
        } else {
          const error = new Error(response.statusText);
          error.response = response;
          dispatch(signupFailed(error));
          throw error;
        }
      })
      .catch((error) => {
        console.log("request failed", error);
      });
  };
}

export function updateRequest(email, username, avatar) {
  const user = { email, username, avatar };
  return { user, type: userConstants.UPDATE_REQUEST };
}

export function updateFailed(error) {
  return { error, type: userConstants.UPDATE_FAILED };
}

export function updateSuccess(response) {
  return (dispatch) => {
    const user = dispatch({
      user: userBoundary(response),
      type: userConstants.UPDATE,
    });
  };
}

export function update(userData) {
  const user = {
    userId: { domain: userData.domain, email: userData.email },
    username: userData.username,
    role: userData.role,
    avatar: userData.avatar,
  };
  return (dispatch, getState) => {
    dispatch(updateRequest(userData.email, userData.username, userData.avatar));
    axios
      .put(
        `/acs/users/${userData.existingUser.domain}/${userData.existingUser.email}`,
        user
      )
      .then((response) => {
        if (response.status >= 200 && response.status < 300) {
          dispatch(updateSuccess(user));
        } else {
          const error = new Error(response.statusText);
          error.response = response;
          dispatch(updateFailed(error));
          throw error;
        }
      })
      .catch((error) => {
        console.log("request failed", error);
      });
  };
}
