import axios from "../helpers/axiosHelper";
import { elementBoundary } from "../boundaries/elementBoundary";
import { elementConstants } from "../constants/elementConstants";

export function setVisibilityFilter(filter, search) {
  return {
    type: "SET_VISIBILITY_FILTER",
    filter,
    search,
  };
}

export function addElementSuccess(el) {
  const newElement = elementBoundary(el);
  return { newElement, type: elementConstants.ADD_ELEMENT };
}

export function addElementFailure(error) {
  return { error, type: elementConstants.ADD_ELEMENT_FAILURE };
}

export function addElement(elementData, userData) {
  return (dispatch, getState) => {
    axios
      .post(`/acs/elements/${userData.domain}/${userData.email}`, {
        name: elementData.name,
        type: elementData.type,
        elementAttributes: elementData.elementAttributes,
        active: elementData.active,
        location: destructLocation(elementData.location),
      })
      .then((response) => {
        if (response.status >= 200 && response.status < 300) {
          dispatch(addElementSuccess(response.data));
        } else {
          const error = new Error(response.statusText);
          error.response = response;
          dispatch(addElementFailure(error));
          throw error;
        }
      })
      .catch((error) => {
        console.log("request failed", error);
      });
  };
}

const destructLocation = (location) => {
  let arr = location.split(",");
  let newLocation = {
    lat: arr[0],
    lng: arr[1],
  };

  return newLocation;
};
