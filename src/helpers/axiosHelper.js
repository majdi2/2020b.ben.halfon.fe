import axios from "axios";

axios.defaults.baseURL = "http://localhost:8092/";
axios.defaults.headers.common["Accept"] = "application/json";
axios.defaults.headers.post["Content-Type"] = "application/json";
axios.defaults.headers.post["Access-Control-Allow-Origin"] = "*";

export default axios;
