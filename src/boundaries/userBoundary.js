export const userBoundary = (res) => {
  return {
    username: res.username,
    role: res.role,
    avatar: res.avatar,
    email: res.userId.email,
    domain: res.userId.domain,
  };
};
