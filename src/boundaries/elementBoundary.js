export const elementBoundary = (res) => {
  let lat = res.location.lat.toString();
  let lng = res.location.lng.toString();
  let loc = lat + "," + lng;
  return {
    name: res.name,
    type: res.type,
    active: res.active,
    location: loc,
    elementAttributes: {
      description:
        res.elementAttributes.description === undefined
          ? ""
          : res.elementAttributes.description,
      reports:
        res.elementAttributes.reports === undefined
          ? ""
          : res.elementAttributes.reports,
    },
    id: res.elementId.id,
    domain: res.elementId.domain,
  };
};
