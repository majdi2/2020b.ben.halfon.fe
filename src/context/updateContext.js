import { createContext } from "react";

const UpdateContext = createContext();

export const UpdateProvider = UpdateContext.Provider;
export const UpdateConsumer = UpdateContext.Consumer;
export default UpdateContext;
