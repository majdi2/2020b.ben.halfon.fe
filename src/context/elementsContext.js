import { createContext } from "react";

const LoadingContext = createContext();

export const Provider = LoadingContext.Provider;
export const Consumer = LoadingContext.Consumer;
export default LoadingContext;
