![iClean](./src/resources/images/elements.jpeg)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm install`

Installs all Dependencies found in package.json file.

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser. (It should open automatically)

The page will reload if you make changes.
Errors from the server are displayed in the console (F12)

### Software and Packages versions:

`react`: 16.9.0
`react-bootstrap`: 16.9.0
`redux`: 4.0.5
`axios`: 0.19.2
`bootstrap`: 4.3.1
